import sqlite3
conn = sqlite3.connect('data.db')
cur = conn.cursor()


def insert_certificateur():
    cur.execute("""INSERT INTO certificateur (libelle_certificateur)
SELECT DISTINCT certificateur
FROM onglet3;""")
    conn.commit()


def insert_diplome():
    cur.execute("""INSERT INTO diplome (libelle_diplome)
SELECT DISTINCT diplome
FROM onglet3;""")
    conn.commit()


def insert_statut():
    cur.execute("""INSERT INTO statut (libelle_statut)
    SELECT DISTINCT statut
    FROM onglet3;""")
    conn.commit()


def insert_npec():
    cur.execute("""INSERT INTO npec (npec_montant, npec_date)
    SELECT npec, date_npec 
    FROM onglet3;""")
    conn.commit()


def insert_cpne():
    cur.execute("""INSERT INTO cpne (code_cpne, libelle_cpne)
    SELECT DISTINCT code_cpne, cpne
    FROM onglet3;""")
    conn.commit()


def insert_idcc():
    cur.execute("""INSERT
    INTO
    idcc(id_cpne, code_idcc)
    SELECT
    onglet4.code_cpne, onglet4.idcc
    FROM
    onglet4;""")
    conn.commit()


def insert_rncp():
    cur.execute("""INSERT INTO rncp (code_rncp, libelle_formation, id_diplome, id_certificateur)
    SELECT code_rncp, libelle_formation, diplome.id, certificateur.id FROM onglet3
    JOIN diplome ON libelle_diplome = onglet3.diplome
    JOIN certificateur ON libelle_certificateur = onglet3.certificateur;
    """)
    conn.commit()


def insert_relation():
    cur.execute("""INSERT INTO relations (id_rncp, id_cpne, id_npec, id_status)
SELECT rncp.id, cpne.code_cpne, npec.id, statut.id FROM onglet3
JOIN rncp ON rncp.code_rncp = onglet3.code_rncp
JOIN cpne ON cpne.libelle_cpne  =  onglet3.cpne
JOIN npec ON npec.npec_montant = onglet3.npec
JOIN statut ON statut.libelle_statut = onglet3.statut;""")
    conn.commit()

