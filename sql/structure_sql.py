import sqlite3

conn = sqlite3.connect('data.db', check_same_thread=False)
cur = conn.cursor()


def sql_structure_certificateur():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'certificateur' (
    'id'	INTEGER,
    'libelle_certificateur'	TEXT NOT NULL,
    PRIMARY KEY (id AUTOINCREMENT)
);""")
    conn.commit()


def sql_structure_statut():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'statut'(
    'id' INTEGER,
    'libelle_statut' TEXT NOT NULL,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()


def sql_structure_diplome():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'diplome'(
    'id' INTEGER,
    'libelle_diplome' TEXT NOT NULL,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()


def sql_structure_idcc():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'idcc'(
    'code_idcc' INTEGER,
    'id_cpne' INTEGER,
    FOREIGN KEY (id_cpne) REFERENCES cpne(id),
    PRIMARY KEY (code_idcc)
 );""")
    conn.commit()


def sql_structure_cpne():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'cpne'(
    'code_cpne' INTEGER,
    'libelle_cpne' TEXT NOT NULL,
    PRIMARY KEY (code_cpne)
 );""")
    conn.commit()


def sql_structure_npec():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'npec'(
    'id' INTEGER,
    'npec_montant' INTEGER,
    'npec_date' DATE,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()


def sql_structure_rncp():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'rncp'(
    'id' INTEGER,
    'code_rncp' TEXT NOT NULL,
    'libelle_formation' TEXT,
    'id_diplome' INTEGER,
    'id_certificateur' INTEGER,
    FOREIGN KEY (id_diplome) REFERENCES diplome(id),
    FOREIGN KEY (id_certificateur) REFERENCES certificateur(id),
    PRIMARY KEY (id AUTOINCREMENT)
    );""")
    conn.commit()


def sql_structure_relations():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'relations'(
    'id_rncp' INTEGER,
    'id_cpne' INTEGER,
    'id_npec' INTEGER,
    'id_status' INTEGER,
    FOREIGN KEY (id_rncp) REFERENCES rncp(id),
    FOREIGN KEY (id_cpne) REFERENCES cpne(id),
    FOREIGN KEY (id_npec) REFERENCES npec(id),
    FOREIGN KEY (id_status) REFERENCES statut(id)
 );""")
    conn.commit()
