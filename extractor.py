import pandas as pd
import sqlite3
import cProfile

from sql.structure_sql import (sql_structure_idcc, sql_structure_npec,
                               sql_structure_certificateur, sql_structure_cpne,
                               sql_structure_diplome, sql_structure_relations,
                               sql_structure_rncp, sql_structure_statut, )
from sql.sql_tables import (insert_certificateur, insert_diplome,
                            insert_idcc, insert_statut, insert_npec,
                            insert_cpne, insert_rncp, insert_relation)


def extractor():
    conn = sqlite3.connect("data.db")
    cur = conn.cursor

    # Extraction des données du fichier excel
    filename = "./ressources/Referentiel-des-NPEC.2024.xlsx"
    print("Lecture ONGLET4")
    df = pd.read_excel(io=filename, engine='openpyxl', names=["code_cpne", "cpne", "idcc"],
                       sheet_name='Onglet 4 - CPNE-IDCC', skiprows=2)
    print("Lecture ONGLET3")
    df2 = pd.read_excel(io=filename, engine='openpyxl', names=["code_rncp", "libelle_formation",
                                                               "certificateur", "diplome", "code_cpne", "cpne",
                                                               "npec", "statut", "date_npec"],
                        sheet_name='Onglet 3 - référentiel NPEC', skiprows=3, nrows=200000)

    # insert des  datas onglet 4 dans une table temporaire
    print("# insert des  datas onglet 4 dans une table temporaire")
    df.to_sql("onglet4", conn, if_exists="replace")

    # insert des datas onglet 3 dans une table temporaire
    print("# insert des datas onglet 3 dans une table temporaire")
    df2.to_sql("onglet3", conn, if_exists="replace")

    # creation des tables définitives
    print("Creation STRUCTURE ")
    sql_structure_certificateur()
    sql_structure_statut()
    sql_structure_cpne()
    sql_structure_idcc()
    sql_structure_diplome()
    sql_structure_npec()
    sql_structure_rncp()
    sql_structure_relations()
    print("Creation STRUCTURE COMPLETED")
    # insertion des données dans les tables definitives
    print("# insertion des données dans les tables definitives")
    print("Certificateur")
    insert_certificateur()
    print('Diplome')
    insert_diplome()
    print("Statut")
    insert_statut()
    print("NPEC")
    insert_npec()
    print("CPNE")
    insert_cpne()
    print("IDCC")
    insert_idcc()
    print("RNCP")
    insert_rncp()
    print("Relation")
    insert_relation()


if __name__ == "__main__":
    with cProfile.Profile() as profile:
        profile.enable()
        extractor()
        profile.disable()
        profile.print_stats()
